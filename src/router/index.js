import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/MainPage'
import Form470 from '@/components/Form470'
import Form471 from '@/components/Form471'
import Form486 from '@/components/Form486'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: MainPage
    },
    {
      path: '/form470/:id',
      name: 'Form470',
      component: Form470
    },
    {
      path: '/form471',
      name: 'Form471',
      component: Form471,
      props: true
    },
    {
      path: '/form486',
      name: 'Form486',
      component: Form486,
      props: true
    }
  ]
})
